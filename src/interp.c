#include "interp.h"
#include "symtable.h"
#include "funtable.h"

#include <stdlib.h>
#include <stdarg.h>

#define MAX_NESTING 20

extern struct Functions_table *fun_table;

static int nesting = 0;
static struct Symbols_table *sym_table[MAX_NESTING] = {NULL};

static void warning(const char *message, ...) {
	fprintf(stderr, "warning: ");
	va_list args;
	va_start(args, message);
	vfprintf(stderr, message, args);
	va_end(args);
	fprintf(stderr, "\n");
}

static void error(const char *message, ...) {
	fprintf(stderr, "[line ??] Runtime error: ");
	va_list args;
	va_start(args, message);
	vfprintf(stderr, message, args);
	va_end(args);
	fprintf(stderr, "\n");
	exit(1);
}

static void errorAt(const htoken *tok, const char *message, ...) {
	fprintf(stderr, "[line %d] Runtime error: ", tok->line);
	va_list args;
	va_start(args, message);
	vfprintf(stderr, message, args);
	va_end(args);
	fprintf(stderr, "\n");
	exit(1);
}

static void add_environment(void) {
	nesting++;
	if (nesting == MAX_NESTING) {
		error("Too much recursion.");
	}
}

void remove_environment(void) {
	if (nesting == 0) {
		error("Returning from after MAIN?");
	}
	clean_table(&sym_table[nesting]);
	nesting--;
}

static Value lookUp(const char *name) {
	int i = nesting;
	for (i = nesting; i >= 0; i--) {
		struct Symbols_table *entry = find_symbol(&sym_table[i], name);
		if (entry != NULL) {
			return entry->value;
		}
	}
	warning("use of uninitialized variable: %s", name);
	return 0;
}

static void assign(const char *name, Value value) {
	add_symbol(&sym_table[nesting], name, value);
}

static Value execute_call(hnode *call);
static Value eval_unary(hnode *unary);
static Value eval_binary(hnode *binary);

static Value dispatch_expr(hnode *expr) {
	switch (expr->type) {
		case HCONST_INT: return expr->token.value.integer;
		case HVAR: return lookUp(expr->token.value.str);
		case HEXPR_UN: return eval_unary(expr);
		case HEXPR_BIN: return eval_binary(expr);
		case HEXPR_CALL: return execute_call(expr);
			break;

		case HCONST_STR:
			errorAt(&expr->token, "Trying to evaluate a string.");
			break;
		case HDECL_FUN:
			errorAt(&expr->token, "Unexpected function declaration.");
			break;
		case HSTMT_WRITE:
		case HSTMT_ASSIGN:
		case HSTMT_READ:
		case HSTMT_IF:
		case HSTMT_WHILE:
		case HSTMT_DO:
			errorAt(&expr->token, "Found statement where expression was expected.");
			break;
		default:
			errorAt(&expr->token, "Unknown node.");
			break;
	}
	return 0;
}

static Value eval_unary(hnode *unary) {
	Value val = dispatch_expr(unary->val.expr);
	if (unary->token.type == HTOK_MINUS)
		return -val;
	else
		return val;
}

static Value eval_binary(hnode *binary) {
	Value left = dispatch_expr(binary->val.left);
	Value right = dispatch_expr(binary->val.right);
	switch (binary->token.type) {
		case HTOK_PLUS:       return left + right;
		case HTOK_MINUS:      return left - right;
		case HTOK_STAR:       return left * right;
		case HTOK_SLASH:      return left / right;
		case HTOK_PERC:       return left % right;
		case HTOK_LESS_EQ:    return left <= right;
		case HTOK_LESS:       return left < right;
		case HTOK_EQUAL:      return left == right;
		case HTOK_GREATER_EQ: return left >= right;
		case HTOK_GREATER:    return left > right;
		case HTOK_BANG_EQ:    return left != right;
		default:
			errorAt(&binary->token, "Unrecognized binary operator.\n");
			break;
	}
	return 0;
}

static Value execute_call(hnode *call) {
	struct Functions_table *fun = find_function(&fun_table, call->token.value.str);
	if (fun == NULL) {
		errorAt(&call->token, "function %s not found.", call->token.value.str);
	}

	if (call->val.paramlen != fun->node->val.arity) {
		errorAt(&call->token, "mismatched number of parameters, expected %zu got %zu.\n", fun->node->val.arity, call->val.paramlen);
	}

	add_environment();
	for (size_t i = 0; i < call->val.paramlen; i++)
		assign(fun->node->val.args[i].value.str, dispatch_expr(call->val.params[i]));

	interpret(fun->node);
	Value retval = lookUp(fun->name);
	remove_environment();
	return retval;
}

static void execute_write(hnode *stmt) {
	if (stmt->val.var_or_str.type == HTOK_STR)
		printf("%s\n", stmt->val.var_or_str.value.str);
	else
		printf("%d\n", lookUp(stmt->val.var_or_str.value.str));
}

static void execute_assign(hnode *stmt) {
	Value val = dispatch_expr(stmt->val.expr);
	assign(stmt->token.value.str, val);
}

static void execute_read(hnode *stmt) {
	Value value;
	for (size_t i = 0; i < stmt->val.readlen; i++) {
		scanf("%d", &value);
		assign(stmt->val.readparams[i].value.str, value);
	}
}

static void dispatch_stmt(hnode *stmt);

static void execute_if(hnode *stmt) {
	Value cond = dispatch_expr(stmt->val.hif.cond);

	if (cond) {
		for (size_t i = 0; i < stmt->val.hif.thenlen; i++)
			dispatch_stmt(stmt->val.hif.thenbody[i]);
	} else {
		for (size_t i = 0; i < stmt->val.hif.elselen; i++)
			dispatch_stmt(stmt->val.hif.elsebody[i]);
	}
}

static void execute_while(hnode *stmt) {
	while (dispatch_expr(stmt->val.loop.cond)) {
		for (size_t i = 0; i < stmt->val.loop.bodylen; i++)
			dispatch_stmt(stmt->val.loop.body[i]);
	}
}

static void execute_do(hnode *stmt) {
	do {
		for (size_t i = 0; i < stmt->val.loop.bodylen; i++)
			dispatch_stmt(stmt->val.loop.body[i]);
	} while (dispatch_expr(stmt->val.loop.cond));
}

static void dispatch_stmt(hnode *stmt) {
	switch (stmt->type) {
		case HSTMT_WRITE:  execute_write(stmt); break;
		case HSTMT_ASSIGN: execute_assign(stmt); break;
		case HSTMT_READ:   execute_read(stmt); break;
		case HSTMT_IF:     execute_if(stmt); break;
		case HSTMT_WHILE:  execute_while(stmt); break;
		case HSTMT_DO:     execute_do(stmt); break;
		case HEXPR_CALL:   execute_call(stmt); break;

		case HDECL_FUN:
			errorAt(&stmt->token, "Unexpected function declaration.");
			break;
		case HCONST_INT:
		case HCONST_STR:
		case HVAR:
		case HEXPR_UN:
		case HEXPR_BIN:
			errorAt(&stmt->token, "Found expression where statement was expected.");
			break;
		default:
			errorAt(&stmt->token, "Unkonwn node!");
			break;
	}
}

void interpret(hnode *main) {
	for (size_t i = 0; i < main->val.bodylen; i++)
		dispatch_stmt(main->val.body[i]);
}

