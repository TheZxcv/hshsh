#include "symtable.h"

static int ID = 0;

void add_symbol(struct Symbols_table **table, const char *name, int value) {
    struct Symbols_table *old = NULL;
    struct Symbols_table *ft;

    ft = malloc(sizeof(struct Symbols_table));
    ft->id = ID++;
    ft->value = value;
    strcpy(ft->name, name);

    HASH_REPLACE_STR(*table, name, ft, old);
	if (old != NULL)
		free(old);
}

struct Symbols_table *find_symbol(struct Symbols_table **table, const char *name) {
    struct Symbols_table *ft;

    HASH_FIND_STR(*table, name, ft);

    return ft;
}

void clean_table(struct Symbols_table **table) {
	static struct Symbols_table *curr, *tmp;
	HASH_ITER(hh, *table, curr, tmp) {
		HASH_DEL(*table, curr);
		free(curr);
	}
	HASH_CLEAR(hh, *table);
}
