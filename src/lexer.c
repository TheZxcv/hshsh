#include "lexer.h"
#include "hshsh.h"

#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>
#include <string.h>

static int line;
static FILE *stream;

void lexer_init(FILE *fp) {
	stream = fp;
	line = 1;
}

static void error(const char *message) {
	fprintf(stderr, "[line %d] error: %s\n", line, message);
	exit(1);
}

static bool isEOF(void) {
	return feof(stream);
}

static char advance(void) {
	if (isEOF()) return '\0';
	return (char) fgetc(stream);
}

static char peek(void) {
	if (isEOF()) return '\0';
	int c = fgetc(stream);
	ungetc(c, stream);
	return (char) c;
}

static bool match(char ch) {
	if (peek() == ch) {
		advance();
		return true;
	} else {
		return false;
	}
}

static void consume(char ch, const char *message) {
	if (peek() == ch) {
		advance();
	} else {
		error(message);
	}
}

void discardWhitespaces(void) {
	bool skip;
	do {
		switch(peek()) {
			case '\n':
				line++;
			case ' ':
			case '\t':
			case '\r':
				skip = true;
				advance();
				break;
			default:
				skip = false;
				break;
		}
	} while (skip);
}

static htoken make_htoken(htoken_type type) {
	htoken tok = { .type = type, .line = line };
	return tok;
}

static htoken digit(void) {
	static char digits[20];
	int i = 0;
	while (isdigit(peek()) && i < 19)
		digits[i++] = advance();
	digits[i] = '\0';
	while (isdigit(peek())) advance();

	htoken tok = make_htoken(HTOK_INT);
	tok.value.integer = atoi(digits);
	return tok;
}

static char chars[16];
static htoken ident(void) {
	int i = 0;
	while (isalnum(peek()) && i < 15)
		chars[i++] = advance();
	chars[i] = '\0';
	while (isalnum(peek())) advance();

	if(strcmp(chars, STR_READ) == 0) {
		return make_htoken(HTOK_READ);
	} else if (strcmp(chars, STR_WRITE) == 0) {
		return make_htoken(HTOK_WRITE);
	} else if (strcmp(chars, STR_IF) == 0) {
		return make_htoken(HTOK_IF);
	} else if (strcmp(chars, STR_THEN) == 0) {
		return make_htoken(HTOK_THEN);
	} else if (strcmp(chars, STR_ELSE) == 0) {
		return make_htoken(HTOK_ELSE);
	} else if (strcmp(chars, STR_ENDIF) == 0) {
		return make_htoken(HTOK_ENDIF);
	} else if (strcmp(chars, STR_WHILE) == 0) {
		return make_htoken(HTOK_WHILE);
	} else if (strcmp(chars, STR_DO) == 0) {
		return make_htoken(HTOK_DO);
	} else if (strcmp(chars, STR_REPEAT) == 0) {
		return make_htoken(HTOK_REPEAT);
	} else if (strcmp(chars, STR_AND) == 0) {
		return make_htoken(HTOK_AND);
	} else if (strcmp(chars, STR_OR) == 0) {
		return make_htoken(HTOK_OR);
	} else if (strcmp(chars, STR_NOT) == 0) {
		return make_htoken(HTOK_NOT);
	} else if (strcmp(chars, STR_BEGIN) == 0) {
		return make_htoken(HTOK_BEGIN);
	} else if (strcmp(chars, STR_END) == 0) {
		return make_htoken(HTOK_END);
	} else if (strcmp(chars, STR_FUNCTION) == 0) {
		return make_htoken(HTOK_FUNCTION);
	} else if (strcmp(chars, STR_GOTO) == 0) {
		return make_htoken(HTOK_GOTO);
	}

	htoken tok = make_htoken(HTOK_IDENT);
	memcpy(tok.value.str, chars, strlen(chars)*sizeof(char));
	return tok;
}

static htoken string(void) {
	int i = 0;
	char ch;
	while ((ch = peek()) !=  '"' && ch != '\n' && i < 15)
		chars[i++] = advance();
	chars[i] = '\0';
	while ((ch = peek()) !=  '"' && ch != '\n') advance();
	consume('"', "Unmatched '\"'.");

	htoken tok = make_htoken(HTOK_STR);
	memcpy(tok.value.str, chars, strlen(chars)*sizeof(char));
	return tok;
}

htoken __lexer_next(void) {
	if (isEOF())
		return make_htoken(HTOK_EOF);
	discardWhitespaces();

	if (isdigit(peek())) return digit();
	if (isalpha(peek())) return ident();

	switch (advance()) {
		case '+': return make_htoken(HTOK_PLUS);
		case '-': return make_htoken(HTOK_MINUS);
		case '*': return make_htoken(HTOK_STAR);
		case '/': return make_htoken(HTOK_SLASH);
		case '%': return make_htoken(HTOK_PERC);
		case '<': return make_htoken(match('-') ? HTOK_ARROW : (match('=') ? HTOK_LESS_EQ : HTOK_LESS));
		case '=': consume('=', "Expect second '=' after '='."); return make_htoken(HTOK_EQUAL);
		case '>': return make_htoken(match('=') ? HTOK_GREATER_EQ : HTOK_GREATER);
		case '!': consume('=', "Expect second '=' after '!'."); return make_htoken(HTOK_BANG_EQ);
		case ',': return make_htoken(HTOK_COMMA);
		case '"': return string();
		case '(': return make_htoken(HTOK_LPAREN);
		case ')': return make_htoken(HTOK_RPAREN);
		case '\0': return make_htoken(HTOK_EOF);
		default: error("Unrecognized character.");
				 break;
	}

	return make_htoken(HTOK_UNKNOWN);
}

void print_htoken(htoken tok) {
#define o(name) case name: printf("%s", #name); break;
	switch (tok.type) {
		case HTOK_EOF: printf("EOF"); break;
		case HTOK_INT: printf("INT(%d)", tok.value.integer); break;
		case HTOK_IDENT:printf("ID(%s)", tok.value.str); break;
		case HTOK_STR:printf("STR(%s)", tok.value.str); break;
		o(HTOK_PLUS)
		o(HTOK_MINUS)
		o(HTOK_STAR)
		o(HTOK_SLASH)
		o(HTOK_PERC)
		o(HTOK_ARROW)
		o(HTOK_LESS_EQ)
		o(HTOK_LESS)
		o(HTOK_EQUAL)
		o(HTOK_GREATER_EQ)
		o(HTOK_GREATER)
		o(HTOK_BANG_EQ)
		o(HTOK_READ)
		o(HTOK_WRITE)
		o(HTOK_IF)
		o(HTOK_THEN)
		o(HTOK_ELSE)
		o(HTOK_ENDIF)
		o(HTOK_WHILE)
		o(HTOK_DO)
		o(HTOK_REPEAT)
		o(HTOK_AND)
		o(HTOK_OR)
		o(HTOK_NOT)
		o(HTOK_BEGIN)
		o(HTOK_END)
		o(HTOK_FUNCTION)
		o(HTOK_GOTO)
		o(HTOK_COMMA)
		o(HTOK_LPAREN)
		o(HTOK_RPAREN)
		default: printf("Unknown"); break;
	}
#undef o
}

htoken lexer_next(void) {
	htoken tok = __lexer_next();
	//print_htoken(tok);
	return tok;
}
