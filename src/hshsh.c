#include "hshsh.h"
#include "lexer.h"
#include "parser.h"
#include "interp.h"

FILE *fp;
struct Functions_table *fun_table = NULL;

int main(int argc, char *argv[]) {

	if(argc == 1) {
		parser_init(stdin);
		parser_parse();
		return EXIT_SUCCESS;
	} else if (argc > 1) {
		fp = fopen(argv[1], "rb");
		if(fp == NULL) {
			printf("Error while opening file\n");
			return EXIT_FAILURE;
		}

		if(flock(fileno(fp), LOCK_EX)) {
			printf("Error while locking file\n");
			return EXIT_FAILURE;
		}
	
		parser_init(fp);
		parser_parse();
		printf("found %u functions\n", HASH_COUNT(fun_table));

		struct Functions_table *ft;
		for(ft = fun_table; ft != NULL; ft = ft->hh.next) {
			printf("Name: %s/%i\n", ft->name, ft->n_args);
			printf("[id: %03i] [line: %04i]\n", ft->id, ft->location);
			printf("[len: %02zu]\n", ft->node->val.bodylen);
		}

		struct Functions_table *fn_main = find_function(&fun_table, "MAIN");
		if (fn_main != NULL) {
			interpret(fn_main->node);
		} else {
			fprintf(stderr, "Missing main function.\n");
		}
		
		struct Functions_table *curr, *tmp;
		HASH_ITER(hh, fun_table, curr, tmp) {
			HASH_DEL(fun_table, curr);
			hfree_ast(curr->node);
			free(curr);
		}

		fflush(stdout);
		fflush(fp);
		//fdatasync(fileno(fp));
		flock(fileno(fp), LOCK_UN);
		fclose(fp);
	}

	return EXIT_SUCCESS;
}

void usage(void) {
	printf("Usage: hshsh file.hs\n");
}
