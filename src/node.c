#include "node.h"
#include "halloc.h"

hnode *make_hnode(hnode_type type, htoken tok) {
	hnode *node = hallocate(1, sizeof(hnode));
	node->type = type;
	node->token = tok;
	return node;
}

hnode *make_const(htoken tok) {
	hnode *node = make_hnode(HCONST_INT, tok);
	return node;
}

hnode *make_var(htoken tok) {
	return make_hnode(HVAR, tok);
}

hnode *make_binary(htoken tok) {
	hnode *node = make_hnode(HEXPR_BIN, tok);
	node->val.left = NULL;
	node->val.right = NULL;
	return node;
}

hnode *make_unary(htoken tok) {
	hnode *node = make_hnode(HEXPR_UN, tok);
	node->val.expr = NULL;
	return node;
}

hnode *make_call(htoken tok) {
	hnode *node = make_hnode(HEXPR_CALL, tok);
	node->val.paramlen = 0;
	node->val.params = NULL;
	return node;
}

hnode *make_fundecl(htoken tok) {
	hnode *node = make_hnode(HDECL_FUN, tok);
	node->val.arity = 0;
	node->val.args = NULL;
	node->val.body = NULL;
	node->val.bodylen = 0;
	return node;
}

hnode *make_assign(htoken name, hnode *expr) {
	hnode *node = make_hnode(HSTMT_ASSIGN, name);
	node->val.expr = expr;
	return node;
}

hnode *make_read(htoken tok) {
	hnode *node = make_hnode(HSTMT_READ, tok);
	node->val.readlen = 0;
	node->val.readparams = NULL;
	return node;
}

hnode *make_write(htoken tok, htoken param) {
	hnode *node = make_hnode(HSTMT_WRITE, tok);
	node->val.var_or_str = param;
	return node;
}

hnode *make_if(htoken tok) {
	hnode *node = make_hnode(HSTMT_IF, tok);
	node->val.hif.cond = NULL;
	node->val.hif.thenlen = 0;
	node->val.hif.thenbody = NULL;
	node->val.hif.elselen = 0;
	node->val.hif.elsebody = NULL;
	return node;
}

hnode *make_while(htoken tok) {
	hnode *node = make_hnode(HSTMT_WHILE, tok);
	node->val.loop.cond = NULL;
	node->val.loop.bodylen = 0;
	node->val.loop.body = NULL;
	return node;
}

hnode *make_do(htoken tok) {
	hnode *node = make_hnode(HSTMT_DO, tok);
	node->val.loop.cond = NULL;
	node->val.loop.bodylen = 0;
	node->val.loop.body = NULL;
	return node;
}

void hfree_ast(hnode *tree) {
	if (!tree)
		return;

	switch (tree->type) {
		case HCONST_INT:
		case HCONST_STR:
		case HVAR:
		case HSTMT_WRITE:
			hfree(tree);
			break;
		case HEXPR_UN:
			hfree_ast(tree->val.expr);
			hfree(tree);
			break;
		case HEXPR_BIN:
			hfree_ast(tree->val.left);
			hfree_ast(tree->val.right);
			hfree(tree);
			break;
		case HEXPR_CALL:
			for (size_t i = 0; i < tree->val.paramlen; i++)
				hfree_ast(tree->val.params[i]);
			hfree(tree->val.params);
			hfree(tree);
			break;
		case HDECL_FUN:
			for (size_t i = 0; i < tree->val.bodylen; i++)
				hfree_ast(tree->val.body[i]);
			hfree(tree->val.body);
			hfree(tree->val.args);
			hfree(tree);
			break;
		case HSTMT_ASSIGN:
			hfree_ast(tree->val.expr);
			hfree(tree);
			break;
		case HSTMT_READ:
			hfree(tree->val.readparams);
			hfree(tree);
			break;
		case HSTMT_IF:
			hfree_ast(tree->val.hif.cond);
			for (size_t i = 0; i < tree->val.hif.thenlen; i++)
				hfree_ast(tree->val.hif.thenbody[i]);
			hfree(tree->val.hif.thenbody);
			if (tree->val.hif.elselen > 0) {
				for (size_t i = 0; i < tree->val.hif.elselen; i++)
					hfree_ast(tree->val.hif.elsebody[i]);
				hfree(tree->val.hif.elsebody);
			}
			hfree(tree);
			break;
		case HSTMT_WHILE:
		case HSTMT_DO:
			hfree_ast(tree->val.loop.cond);
			for (size_t i = 0; i < tree->val.loop.bodylen; i++)
				hfree_ast(tree->val.loop.body[i]);
			hfree(tree->val.loop.body);
			hfree(tree);
			break;
		default:
			fprintf(stderr, "Unkonwn node!\n");
			break;
	}
}
