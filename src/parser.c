#include "parser.h"
#include "lexer.h"
#include "funtable.h"
#include "node.h"

#include <stdlib.h>
#include <stdbool.h>

extern struct Functions_table *fun_table;

static htoken previous;
static htoken current;

static void warning(htoken tok, const char *message) {
	fprintf(stderr, "[line %d] warning: %s\n", tok.line, message);
}

static void error(htoken tok, const char *message) {
	fprintf(stderr, "[line %d] syntax error: %s\n", tok.line, message);
	exit(1);
}

static void error_on_prev(const char *message) {
	error(previous, message);
}

static htoken peek(void) {
	return current;
}

static htoken advance(void) {
	previous = current;
	current = lexer_next();
	return previous;
}

static bool match(htoken_type type) {
	if (peek().type == type) {
		advance();
		return true;
	} else {
		return false;
	}
}

static bool check(htoken_type type) {
	return peek().type == type;
}

static void consume(htoken_type type, const char *message) {
	htoken tok = peek();
	if (tok.type == type)
		advance();
	else
		error(tok, message);
}

static bool iseof(void) {
	return peek().type == HTOK_EOF;
}

static hnode *condition(void) {
	if(match(HTOK_IDENT) || match(HTOK_INT)) {
		hnode *left = previous.type == HTOK_IDENT ? make_var(previous)
					: make_const(previous);
		switch (peek().type) {
			case HTOK_LESS_EQ:
			case HTOK_LESS:
			case HTOK_EQUAL:
			case HTOK_GREATER_EQ:
			case HTOK_GREATER:
			case HTOK_BANG_EQ: {
				hnode *node = make_binary(peek());
				node->val.left = left;
				advance();
				if(!(match(HTOK_IDENT) || match(HTOK_INT)))
					error(peek(),  "Expect variable name or constant after operator.");

				node->val.right = previous.type == HTOK_IDENT ? make_var(previous)
							: make_const(previous);
				return node;
			}
			default:
				error(peek(),  "Expect comparation operator.");
				return NULL;
		}
	}
	error_on_prev("Expect expression.");
	return NULL;
}

static hnode *statement(void);

static hnode *do_statement(void) {
	hnode *node = make_do(previous);
	while (!check(HTOK_WHILE) &&  !iseof()) {
		node->val.loop.body = realloc(node->val.loop.body, (node->val.loop.bodylen + 1)*sizeof(hnode*));
		node->val.loop.body[node->val.loop.bodylen] = statement();
		node->val.loop.bodylen++;
	}
	consume(HTOK_WHILE, "Expect 'QUANDO' after do-while body.");
	node->val.loop.cond = condition();
	return node;
}

static hnode *while_statement(void) {
	hnode *node = make_while(previous);
	node->val.loop.cond = condition();
	consume(HTOK_DO, "Expect 'ESEGUI' after condition.");

	while (!check(HTOK_REPEAT) &&  !iseof()) {
		node->val.loop.body = realloc(node->val.loop.body, (node->val.loop.bodylen + 1)*sizeof(hnode*));
		node->val.loop.body[node->val.loop.bodylen] = statement();
		node->val.loop.bodylen++;
	}
	consume(HTOK_REPEAT, "Expect 'RIPETI' after while body.");
	return node;
}

static hnode *if_statement(void) {
	hnode *node = make_if(previous);
	node->val.hif.cond = condition();
	consume(HTOK_THEN, "Expect 'ALLORA' after if condition.");

	while (!check(HTOK_ENDIF) && !check(HTOK_ELSE) && !iseof()) {
		node->val.hif.thenbody = realloc(node->val.hif.thenbody, (node->val.hif.thenlen + 1)*sizeof(hnode*));
		node->val.hif.thenbody[node->val.hif.thenlen] = statement();
		node->val.hif.thenlen++;
	}

	if (match(HTOK_ELSE)) {
		while (!check(HTOK_ENDIF) && !iseof()) {
			node->val.hif.elsebody = realloc(node->val.hif.elsebody, (node->val.hif.elselen + 1)*sizeof(hnode*));
			node->val.hif.elsebody[node->val.hif.elselen] = statement();
			node->val.hif.elselen++;
		}
	}
	consume(HTOK_ENDIF, "Expect 'FINESE' after if body.");
	return node;
}

static hnode *write_statement(void) {
	htoken tok = previous;
	if (match(HTOK_STR) || match(HTOK_IDENT))
		return make_write(tok, previous);
	else
		error(peek(), "Expect string or variable name after 'SCRIVI'.");
	return NULL;
}

static hnode *read_statement(void) {
	hnode *node = make_read(previous);
	if (match(HTOK_IDENT)) {
		node->val.readparams = realloc(node->val.readparams, sizeof(htoken));
		node->val.readparams[0] = previous;
		node->val.readlen = 1;

		while (match(HTOK_COMMA) && !iseof()) {
			consume(HTOK_IDENT, "Expect argument after ','.");

			node->val.readparams = realloc(node->val.readparams, (node->val.readlen + 1)*sizeof(htoken));
			node->val.readparams[node->val.readlen] = previous;
			node->val.readlen++;
		}
	}
	return node;
}

static hnode *finish_funcall(htoken name) {
	hnode *node = make_call(name);
	if (match(HTOK_IDENT) || match(HTOK_INT)) {
		node->val.params = realloc(node->val.params, sizeof(htoken));
		node->val.params[0] = previous.type == HTOK_INT ? make_const(previous) : make_var(previous);
		node->val.paramlen = 1;
		while (match(HTOK_COMMA)) {
			node->val.params = realloc(node->val.params, (node->val.paramlen + 1)*sizeof(htoken));
			if (match(HTOK_IDENT))
				node->val.params[node->val.paramlen] = make_var(previous);
			else if(match(HTOK_INT))
				node->val.params[node->val.paramlen] = make_const(previous);
			else
				error(peek(), "Expect argument after ','.");
			node->val.paramlen++;
		}
	}
	consume(HTOK_RPAREN, "Expect ')' after function arguments.");
	return node;
}

static hnode *primary(void) {
	if (match(HTOK_INT)) {
		return make_const(previous);
	} else if(match(HTOK_IDENT)) {
		htoken name = previous;
		if (match(HTOK_LPAREN))
			return finish_funcall(name);
		else
			return make_var(name);
	}

	error(peek(), "Expect expression.");
	return NULL;
}

static hnode *unary(void) {
	if (match(HTOK_PLUS)) {
		return unary();
	} else if (match(HTOK_MINUS)) {
		hnode *node = make_unary(previous);
		node->val.expr = unary();
		return node;
	} else {
		return primary();
	}
}

static hnode *factor(void) {
	hnode *node = unary();

	while (match(HTOK_STAR) || match(HTOK_SLASH) || match(HTOK_PERC)) {
		hnode *expr = make_binary(previous);
		expr->val.left = node;
		expr->val.right = factor();
		node = expr;
	}

	return node;
}

static hnode *term(void) {
	hnode *node = factor();

	while (match(HTOK_PLUS) || match(HTOK_MINUS)) {
		hnode *expr = make_binary(previous);
		expr->val.left = node;
		expr->val.right = term();
		node = expr;
	}

	return node;
}

static hnode *expression(void) {
	return term();
}

static hnode *assignment(htoken name) {
	return make_assign(name, expression());
}

static hnode *statement(void) {
	if (match(HTOK_INT)) {
		// label
		warning(previous, "found label");
	}

	if (match(HTOK_IDENT)) {
		htoken name = previous;
		if (match(HTOK_ARROW)) {
			return assignment(name);
		} else if (match(HTOK_LPAREN)) {
			return finish_funcall(name);
		}
	} else if (match(HTOK_READ)) {
		return read_statement();
	} else if (match(HTOK_WRITE)) {
		return write_statement();
	} else if (match(HTOK_IF)) {
		return if_statement();
	} else if (match(HTOK_WHILE)) {
		return while_statement();
	} else if (match(HTOK_DO)) {
		return do_statement();
	} else if (match(HTOK_GOTO)) {
		error_on_prev("GOTO is not supported and never will.");
	} else {
		print_htoken(peek());
		error(peek(), "Expect statement.");
	}
	return NULL;
}


static hnode *func_declaration(void) {
	hnode *node;
	consume(HTOK_FUNCTION, "Expect 'FUNZIONE'.");
	consume(HTOK_IDENT, "Expect function name after 'FUNZIONE'.");
	node = make_fundecl(previous);

	if (match(HTOK_IDENT)) {
		node->val.arity = 1;
		node->val.args = realloc(node->val.args, sizeof(htoken));
		node->val.args[0] = previous;
		while (match(HTOK_COMMA) && !iseof()) {
			consume(HTOK_IDENT, "Expect parameter name after ','.");
			node->val.args = realloc(node->val.args, (node->val.arity + 1)*sizeof(htoken));
			node->val.args[node->val.arity] = previous;
			node->val.arity++;
		}
	}
	consume(HTOK_BEGIN, "Expect 'INIZIO' after function signature.");

	while (!check(HTOK_END) && !iseof()) {
		node->val.body = realloc(node->val.body, (node->val.bodylen + 1)*sizeof(hnode*));
		node->val.body[node->val.bodylen] = statement();
		node->val.bodylen++;
	}
	consume(HTOK_END, "Expect 'FINE' after function body.");

	return node;
}

void parser_init(FILE *file) {
	lexer_init(file);
	current = lexer_next();
}

void parser_parse(void) {
	while (!iseof()) {
		hnode *fun = func_declaration();
		add_function(&fun_table, fun->token.value.str, fun->token.line, fun->val.arity, fun);
	}
}
