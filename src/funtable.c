#include "funtable.h"

static int ID = 0;

void add_function(struct Functions_table **fun_table, char *name, int loc, int n_args, hnode *node) {
    struct Functions_table *ft;

    ft = malloc(sizeof(struct Functions_table));
    ft->id = ID++;
    ft->location = loc;
    ft->n_args = n_args;
    strcpy(ft->name, name);
    ft->node = node;

    HASH_ADD_STR( *fun_table, name, ft );
}

struct Functions_table *find_function(struct Functions_table **fun_table, char *name) {
    struct Functions_table *ft;

    HASH_FIND_STR( *fun_table, name, ft );

    return ft;
}
