#include "halloc.h"

#include <stdlib.h>

void *hallocate(size_t nelem, size_t size) {
	return calloc(nelem, size);
}

void hfree(void *ptr) {
	free(ptr);
}
