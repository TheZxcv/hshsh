#ifndef HPARSER_H
#define HPARSER_H

#include <stdio.h>

void parser_init(FILE *file);
void parser_parse(void);

#endif
