#ifndef FUNTABLE_H
#define FUNTABLE_H

#include "node.h"
#include "lib/uthash.h"

//static int ID = 0;

struct Functions_table {
	char name[20];      /* the key */
	int id;             /* incremental id used for symbols scope */
	int location;       /* the line where it starts */
	int n_args;         /* number of args it needs */
	hnode *node;        /* function node */
	UT_hash_handle hh;  /* makes this structure hashable */
};

void add_function(struct Functions_table **fun_table, char *name, int loc, int n_args, hnode *node);
struct Functions_table *find_function(struct Functions_table **fun_table, char *name);

#endif	//FUNTABLE_H
