#ifndef SYMTABLE_H
#define SYMTABLE_H

#include "lib/uthash.h"

struct Symbols_table {
	int id;
	char name [20];
	int value;
	UT_hash_handle hh;	/* makes this structure hashable */
};

void clean_table(struct Symbols_table **table);
void add_symbol(struct Symbols_table **table, const char *name, int value);
struct Symbols_table *find_symbol(struct Symbols_table **table, const char *name);

#endif	//SYMTABLE_H
