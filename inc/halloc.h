#ifndef HHALLOC_H
#define HHALLOC_H

#include <stddef.h>

void *hallocate(size_t nelem, size_t size);
void hfree(void *ptr);

#endif
