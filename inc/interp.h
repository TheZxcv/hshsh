#ifndef HINTERP_H
#define HINTERP_H

#include "node.h"

typedef int Value;

void interpret(hnode *program);

#endif
