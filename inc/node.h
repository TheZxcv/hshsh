#ifndef HNODE_H
#define HNODE_H

#include "lexer.h"

typedef enum {
	HCONST_INT,
	HCONST_STR,
	HVAR,
	HEXPR_UN,
	HEXPR_BIN,
	HEXPR_CALL,
	HDECL_FUN,
	HSTMT_ASSIGN,
	HSTMT_WRITE,
	HSTMT_READ,
	HSTMT_IF,
	HSTMT_WHILE,
	HSTMT_DO,
} hnode_type;

// forward declaration
typedef struct hnode_s hnode;

typedef struct hnode_s {
	htoken token;
	hnode_type type;
	union {
		htoken var_or_str;
		hnode *expr;
		struct {
			hnode *left;
			hnode *right;
		};
		struct {
			size_t arity;
			htoken *args;
			size_t bodylen;
			hnode **body;
		};
		struct {
			size_t paramlen;
			hnode **params;
		};
		struct {
			size_t readlen;
			htoken *readparams;
		};
		struct {
			hnode *cond;
			size_t thenlen;
			hnode **thenbody;
			size_t elselen;
			hnode **elsebody;
		} hif;
		struct {
			hnode *cond;
			size_t bodylen;
			hnode **body;
		} loop;
	} val;
} hnode;

hnode *make_const(htoken tok);
hnode *make_var(htoken tok);
hnode *make_unary(htoken tok);
hnode *make_binary(htoken tok);
hnode *make_call(htoken tok);
hnode *make_fundecl(htoken tok);
hnode *make_assign(htoken name, hnode *expr);

hnode *make_read(htoken tok);
hnode *make_write(htoken tok, htoken param);
hnode *make_if(htoken tok);
hnode *make_while(htoken tok);
hnode *make_do(htoken tok);

void hfree_ast(hnode *tree);

#endif
