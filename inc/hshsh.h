#ifndef HSHSH_H
#define HSHSH_H

#ifdef __linux__
#define _POSIX_C_SOURCE 200809L
#elif WIN32

#else
#error Not Supported
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>	/* isspace(1) */
#include <sys/file.h>
#include <unistd.h>	/* fdatasync(1) */

#include "funtable.h"
#include "symtable.h"


/*enum { LEGGI, SCRIVI, SE, ALLORA, ALTRIMENTI,
	FINESE, QUANDO, ESEGUI, RIPETI, E, O, NON
	MAIN, INIZIO, FINE, FUNZIONE, GOTO
	};
*/
enum { UNKNOWN = -1, READ, WRITE, IF, THEN, ELSE,
	ENDIF, WHILE, DO, REPEAT, AND, OR, NOT,
	MAIN, BEGIN, END, FUNCTION, GOTO
};
#define STR_READ 	"LEGGI"
#define STR_WRITE 	"SCRIVI"
#define STR_IF 		"SE"
#define STR_THEN 	"ALLORA"
#define STR_ELSE 	"ALTRIMENTI"
#define STR_ENDIF 	"FINESE"
#define STR_WHILE 	"QUANDO"
#define STR_DO 		"ESEGUI"
#define STR_REPEAT 	"RIPETI"
#define STR_AND 	"E"
#define STR_OR 		"O"
#define STR_NOT 	"NON"
#define STR_MAIN 	"MAIN"
#define STR_BEGIN 	"INIZIO"
#define STR_END 	"FINE"
#define STR_FUNCTION 	"FUNZIONE"
#define STR_GOTO 	"GOTO"

void usage(void);

#endif	//HSHSH_H
