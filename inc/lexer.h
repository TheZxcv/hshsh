#ifndef HLEXER_H
#define HLEXER_H

#include <stdio.h>

typedef enum {
	HTOK_EOF,
	HTOK_INT,
	HTOK_IDENT,
	HTOK_STR,
	HTOK_PLUS,
	HTOK_MINUS,
	HTOK_STAR,
	HTOK_SLASH,
	HTOK_PERC,
	HTOK_ARROW,
	HTOK_LESS_EQ,
	HTOK_LESS,
	HTOK_EQUAL,
	HTOK_GREATER_EQ,
	HTOK_GREATER,
	HTOK_BANG_EQ,
	HTOK_COMMA,
	HTOK_READ,
	HTOK_WRITE,
	HTOK_IF,
	HTOK_THEN,
	HTOK_ELSE,
	HTOK_ENDIF,
	HTOK_WHILE,
	HTOK_DO,
	HTOK_REPEAT,
	HTOK_AND,
	HTOK_OR,
	HTOK_NOT,
	HTOK_BEGIN,
	HTOK_END,
	HTOK_FUNCTION,
	HTOK_GOTO,
	HTOK_LPAREN,
	HTOK_RPAREN,
	HTOK_UNKNOWN,
} htoken_type;

typedef struct {
	htoken_type type;
	int line;
	union {
		char str[16];
		int integer;
	} value;
} htoken;

void lexer_init(FILE*);
htoken lexer_next(void);
void print_htoken(htoken);

#endif
