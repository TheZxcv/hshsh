CC = gcc
CFLAGS = -Wall -g3 -Wextra -Wno-implicit-fallthrough -fsanitize=undefined,address,leak
INCLUDES = -I. -Iinc/

SOURCEDIR = src
LIBDIR = lib

SRCS = $(wildcard $(SOURCEDIR)/*.c)
OBJS = $(SRCS:.c=.o)
BIN = Hshsh
RM = rm -f

all: $(SRCS) $(BIN)

$(BIN): $(OBJS)
	$(CC) $(CFLAGS) $(INCLUDES) -o $(BIN) $(OBJS)

.c.o:
	$(CC) $(CFLAGS) $(INCLUDES) -c $<  -o $@


clean:
	${RM} *~ ${BIN} ${OBJS}
